<?php
// Application middleware

// e.g: $app->add(new \Slim\Csrf\Guard);

$app->add(new \Slim\Middleware\Session([
    'name' => 'erapis_session',
    'autorefresh' => false,
    'lifetime' => '1 hour',
    'secure'    =>  true
  ]));