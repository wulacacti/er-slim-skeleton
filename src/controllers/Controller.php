<?php
namespace App\Controllers;

use Psr\Container\ContainerInterface;

class Controller
{
    protected $db;

    protected $container;

    protected $view;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->view = $this->container->get('view');
    }
}
