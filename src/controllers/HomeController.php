<?php
namespace App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;

class HomeController extends Controller
{
    function __construct(\Psr\Container\ContainerInterface $container)
    {
        parent::__construct($container);
    }

    public function index(Request $request, Response $response){
        $data = array();
        
        // code here.

        return $this->view->render($response, 'welcome.twig', $data);
    }
}
