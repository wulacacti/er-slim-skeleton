<?php

use Dotenv\Dotenv;
use Gettext\Translator;
use Gettext\Translations;

if (PHP_SAPI == 'cli-server') {
    // To help the built-in PHP dev server, check if the request was actually for
    // something which should probably be served as a static file
    $url  = parse_url($_SERVER['REQUEST_URI']);
    $file = __DIR__ . $url['path'];
    if (is_file($file)) {
        return false;
    }
}

require __DIR__ . '/../vendor/autoload.php';

// we are using bryanjhv/slim-session manage session.
// session_start();

$dotenv = new Dotenv(__DIR__ . '/../');
$dotenv->load();

$t = new Translator();
$trans = Translations::fromJsonFile(__DIR__ . '/../src/langs/en.json');
$t->loadTranslations($trans);
$t->register();

// Instantiate the app
$settings = require __DIR__ . '/../src/settings.php';
$app = new \Slim\App($settings);

// load the common helper functions.

require __DIR__ . '/../src/helpers.php';

// Set up dependencies
require __DIR__ . '/../src/dependencies.php';

// Register middleware
require __DIR__ . '/../src/middleware.php';

// Register routes
require __DIR__ . '/../src/routes.php';

// Run app
$app->run();
