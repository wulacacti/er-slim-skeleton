# Slim Framework 3 Skeleton Application

**This skeleton application based on slim/slim-skeleton project.**

Use this skeleton application to quickly setup and start working on a new Slim Framework 3 application. This application uses the latest Slim 3 with the PHP-View template renderer. It also uses the Monolog logger.

This skeleton application was built for Composer. This makes setting up a new Slim Framework application quick and easy.

## Install the Application

Run this command from the directory in which you want to install your new Slim Framework application.

    php composer.phar create-project ersun/slim-skeleton [my-app-name]

Replace `[my-app-name]` with the desired directory name for your new application. You'll want to:

* Point your virtual host document root to your new application's `public/` directory.
* Ensure `logs/` is web writeable.

To run the application in development, you can run these commands 

	cd [my-app-name]
	php composer.phar start

Run this command in the application directory to run the test suite

	php composer.phar test

That's it! Now go build something cool.

## Extra
I add follows library and framework into it.

**PHP**
* vlucas/phpgetenv for .env file support.
* gettext/gettext for localization support.
* bryanjhv/slim-session a helpful session middleware.
* league/flysystem for filesystem abstraction
* league/flysystem-aws-s3-v3 s3 adapter for flysystem, but mostly I works with DO spaces.


**Javascript**
* Jquery
* Vue

**CSS**
Bootstrap 4